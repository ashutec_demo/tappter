import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { EditProfileComponent } from "../profile/edit-profile/edit-profile.component";
import { ProfileListComponent } from "../profile/profilelist/profilelist.component";

const routes: Routes = [
    { path: "", redirectTo: "profileList", pathMatch: "full"},
    { path: "profileList", component: ProfileListComponent },
    { path: "editProfile", component: EditProfileComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ProfileRoutingModule { }