import { Component } from "@angular/core";
import { SharingService } from "../../services/sharing.service";
import { RouterExtensions } from "nativescript-angular/router";
import * as Toast from 'nativescript-toast';

import { dataItems } from "../utils";

@Component({
    moduleId: module.id,
    selector: "ns-edit-profile",
    templateUrl: "edit-profile.component.html",
    styleUrls: ["edit-profile.component.css"]
})
export class EditProfileComponent {
    list;
    toast = Toast.makeText("Details Updated Successfully");
    
    constructor(private sharingService: SharingService,
        private router: RouterExtensions,
    ) {
    }

    ngOnInit() {
        this.sharingService.profileInfo.subscribe((data) => {
            this.list = data;
        });
    }

// update profile info
    save() {
        var updatedata = dataItems.map(data => {
            if (data.id === this.list.id) {
                return this.list;
            }
            return data
        })
        localStorage.setItem('userList', JSON.stringify(updatedata));
        this.toast.show();
        this.router.navigate(["main/profile/profileList"]);
    }
}