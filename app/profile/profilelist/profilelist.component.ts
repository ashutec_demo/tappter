import { Component, OnInit } from "@angular/core";
import * as TNSPhone from 'nativescript-phone';
import { RouterExtensions } from "nativescript-angular/router";
import * as localStorage from 'nativescript-localstorage';
import { SharingService } from "~/services/sharing.service";
import { dataItems } from "../utils";

@Component({
    moduleId: module.id,
    selector: "ns-profilelist",
    templateUrl: "profilelist.component.html",
    styleUrls: ["profilelist.component.css"]
})
export class ProfileListComponent implements OnInit {
    list: any = [];
    me: any = [];
    constructor(
        private router: RouterExtensions,
        private sharingService: SharingService,
    ) {
        this.me = localStorage.getItem('userList');
        
        if(this.me === null){
            localStorage.setItem('userList', JSON.stringify(dataItems));
            this.list = dataItems;
        } else{
            this.list =  JSON.parse(this.me);
        }
    }

    ngOnInit() {
    }
// redirect to phone dial
    callHome(item) {
        TNSPhone.dial(item.number, true);
    }
// edit page navigation
    EditContact(item) {
        this.sharingService.profileData(item);
        this.router.navigate(["main/profile/editProfile"]);
    }
}