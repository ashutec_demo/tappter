import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { MainComponent } from "./main/main.component";


const routes: Routes = [
    { path: "", redirectTo: "main/contact", pathMatch: "full" },
    {
        path: "main", component: MainComponent,
        children: [
            { path: "", redirectTo: "/contact", pathMatch: "full" },
            {
                path: "contact",
                loadChildren: "./contact/contact.module#ContactModule"
            },
            {
                path: "profile",
                loadChildren: "./profile/profile.module#ProfileModule"
            }
        ]
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }