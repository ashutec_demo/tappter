import { Component } from "@angular/core";
import { BottomNavigation, OnTabSelectedEventData } from 'nativescript-bottom-navigation';
import { RouterExtensions } from "nativescript-angular/router";

import {Page} from "ui/page";

@Component({
    moduleId: module.id,
    selector: "ns-main",
    templateUrl: "main.component.html",
    styleUrls: ["main.component.css"]
})
export class MainComponent {
    public selectedTab: number = 2;
    _bottomNavigation: BottomNavigation;

    constructor(
        private page:Page,
        private router: RouterExtensions
    ){
        this.page.actionBarHidden = true;
    }

    onBottomNavigationTabSelected(args: OnTabSelectedEventData): void {
        this.selectedTab = args.newIndex;
        if (this.selectedTab === 0) {
            this.router.navigate(["main/contact/contactList"]);
        } else {
            this.router.navigate(["main/profile/profileList"]);
        }
    }
}