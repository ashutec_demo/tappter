import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

@Injectable()

export class SharingService {

  // Sharing common data using observable
  private contactSource = new BehaviorSubject('');
  contactInfo = this.contactSource.asObservable();

  private profileSource = new BehaviorSubject('');
  profileInfo = this.profileSource.asObservable();

  contactData(contactData: any) {
    this.contactSource.next(contactData)
  }

  profileData(profileData: any) {
    this.profileSource.next(profileData)
  }
}
