import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ContactDetailsComponent } from "../contact/contact-details/contact-details.component";
import { ContactListComponent } from "../contact/contactlist/contactlist.component";

const routes: Routes = [
    { path: "", redirectTo: "contactList", pathMatch: "full"},
    { path: "contactDetails", component: ContactDetailsComponent },
    { path: "contactList", component: ContactListComponent }

];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ContactRoutingModule { }