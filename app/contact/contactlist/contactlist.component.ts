import { Component, OnInit } from "@angular/core";
import { dataItems } from "../utils";
import { RouterExtensions } from "nativescript-angular/router";
import { SharingService } from "../../services/sharing.service";

@Component({
    moduleId: module.id,
    selector: "ns-contactlist",
    templateUrl: "contactlist.component.html",
    styleUrls: ["contactlist.component.css"]
})
export class ContactListComponent implements OnInit {
    list;
    constructor(
        private sharingService: SharingService,
        private router: RouterExtensions
    ) {
    }
    ngOnInit() {
        this.list = dataItems;
    }
    // Navigate to contact details
    onSelect(items) {
        this.sharingService.contactData(items);
        this.router.navigate(["main/contact/contactDetails"]);
    }

}