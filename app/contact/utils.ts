export const dataItems = [
    { id:"1", name: "Nancy Watkins", age:"18", country:"IN", mobileNumber:"+912864897456", emailAddress:"nancyw@gmail.com", gender:"female" },
    { id:"2", name: "Timothy Watson", age:"25", country:"IN", mobileNumber:"+918951242635", emailAddress:"timothy@gmail.com", gender:"female"},
    { id:"3", name: "Dominic Warren",age:"34", country:"IN", mobileNumber:"+917657541132", emailAddress:"dominicw561@gmail.com", gender:"male"},
    { id:"4", name: "Ina Curtis", age:"29", country:"IN", mobileNumber:"+919895156723", emailAddress:"inacurtis89@gmail.com", gender:"female"},
    { id:"5", name: "Nancy Watkins", age:"38", country:"IN", mobileNumber:"+919658364271", emailAddress:"nancywatkin50@gmail.com", gender:"female"},
];
