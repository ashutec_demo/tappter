import { Component, OnInit } from "@angular/core";
import { SharingService } from "../../services/sharing.service";

@Component({
    moduleId: module.id,
    selector: "ns-contact-details",
    templateUrl: "contact-details.component.html",
    styleUrls: ["contact-details.component.css"]
})
export class ContactDetailsComponent implements OnInit {
    list;
    constructor(private sharingService: SharingService
    ) { }
    ngOnInit() {
        this.sharingService.contactInfo.subscribe((data) => {
            this.list = data;
           
        });
    }
}